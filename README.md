# Time Online React App ⏲

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Log analyzer frontend app
Simple intuitive interface that lets you upload **one or more** log files containing user`s actions in format: **[timestamp] [user_name] [action]**. 

An **optional** parameter **grace period** can be provided in the input field below.

It sends the data to the nestjs backend to be analyzed and can return the results in a desired format.

### Supported formats:
- JSON
- HTML
- XML

## Screenshots 📸
![main-view](./ReadMeFiles/main.PNG)

![main-used](./ReadMeFiles/main-used.png)

![login-form](./ReadMeFiles/login.png)

![see-my-files](./ReadMeFiles/my-files.png)

## Getting Started 🚀

### Development
 
> Requirements: You will need npm from [NodeJs](https://nodejs.org/)

```javascript
npm install //download all dependencies
npm start 
```

>Note: This app is designed to be used with its [NestJs](https://gitlab.com/loganalyzer-fullstack-app-with-react-and-nestjs/nestjs-rest-api) backend. Make sure you are running the backend and database first before making requets from the app. All api routes can be found in `.env`

### Production
In the `.env` file change the variable `REACT_APP_BACKEND_BASE_URL`

### Layout
- All react components are in the `src/componets` dir
- All request making components take as a prop a requestObj form `src/services/SendRequest.ts`. This object handles all requests from the app and stores metadata such as **JWT** tokens.
>Note: `SendRequest` obj is created through a `SendRequestFactory`- it can also return a Mock Object with fake requests for testing

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
