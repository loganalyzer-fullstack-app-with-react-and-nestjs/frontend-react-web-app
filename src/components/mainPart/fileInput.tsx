import React from "react";
import Dropzone, { FileRejection } from "react-dropzone";
import { CONTENT_TYPE, REQUEST, SendRequest } from "../../services/SendRequest";
import './fileUpload.css';

interface FileInputState {
  files: File[] | undefined, 
  gracePeriod: number | undefined,
  uploaded: boolean,
}

export class FileInput extends React.Component<{requestObj: SendRequest, setVisibleLogin(render: boolean): void}, FileInputState> {

  constructor(props: any) {
    super(props);
    this.state = {
      files: undefined,
      gracePeriod: undefined,
      uploaded: false
    }

    this.onDrop = this.onDrop.bind(this);
    this.hasDuplicateValidator = this.hasDuplicateValidator.bind(this);
    this.showFiles = this.showFiles.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.changeStyleFileInput = this.changeStyleFileInput.bind(this);
  }

  private hasDuplicateValidator(file: File) {
    if (this.state.files) {
      for (let f of this.state.files) {
        if (f.name === file.name) {
          return {
            code: 'No-duplicate-files-allowed',
            message: 'No dublicate files allowed'
          }
        }
      }
    }
    return null;
  }

  private onChange(event: any) {
    const target = event.target as HTMLInputElement;
    this.setState({
        gracePeriod: Number(target.value),
    });
  }

  private onDrop(acceptedFiles: File[]) {
    const prevFiles = this.state?.files?.slice();
    console.log(acceptedFiles);
    this.setState({
      files: prevFiles ? prevFiles.concat(acceptedFiles) : acceptedFiles,
    });
  }

  private showErrors(fileRejections: FileRejection[]) {
    return fileRejections.map(({ file, errors }) => (
      <li key={file.name}>
        {errors.map(e => (
          <li key={e.code}>{e.message}</li>
        ))}
      </li>
    ));
  }

  private showFiles() {
    return this.state.files?.map(file => (
      <li key={file.name}>
        {file.name} - {file.size} bytes
      </li>
    ));
  }

  private onSubmit(event: any) {
    let formData = new FormData();
    if(this.state.files) {
      this.state.files.map((file, index) => {
        formData.append(`files`, file);
      });

      formData.append('gracePeriod', this.state.gracePeriod? String(this.state.gracePeriod) : '0');
    }

    const requestStr = `${process.env.REACT_APP_BACKEND_BASE_URL}${process.env.REACT_APP_UPLOAD_FILE_ROUTE}`;
    event.preventDefault();

    const url = new URL(requestStr);
    this.props.requestObj.sendRequest(url, REQUEST.POST, CONTENT_TYPE.FORM_DATA, formData)
    .then(res => {
        //If successfull
        this.setState({
            uploaded: true
        });
        sessionStorage.setItem('fileId', res.fileId);
    }).catch(err => {
        console.log(err);
        if(err.statusCode === 401)
          this.props.setVisibleLogin(true);
        return;
    });
  }

  private changeStyleFileInput(isDragActive: boolean) {
    const shadow = '0px 2px 16px -1px ';
    let style = {};

    let status = '';
    if(this.state.files || isDragActive)
      status = 'Added';
    
    if(this.state.uploaded)
      status = 'Uploaded';

    switch(status) {
      case 'Added':
        style = {
          boxShadow: shadow + 'aqua'
        };
        break;
      
      case 'Uploaded':
        style = {
          boxShadow: shadow + 'green'
        };
        break;
      
      default:
        style = {
          boxShadow: shadow + 'white'
        }
        break;
    }
    return style;
  }

  render() {
    return (
      <div className="dropzone-container">
        <div className="transparent-container">
          <Dropzone onDrop={this.onDrop} multiple validator={this.hasDuplicateValidator}>
            {({ getRootProps, getInputProps, isDragActive, fileRejections }) => (
              <div className="fileInput-box">
                {/* File input */}
                <div {...getRootProps()} className="fileInput" style={this.changeStyleFileInput(isDragActive)}>
                  <input {...getInputProps()} />
                  <div>
                    {isDragActive ? "Drop it like it's hot!" : 'Click me or drag a file to upload!'}
                  </div>
                </div>
                {/* Number form */}
                <form className="general-form" id="fileForm" onSubmit={this.onSubmit}>
                  <input type="number" placeholder="Grace period in seconds" value={this.state.gracePeriod} onChange={this.onChange}/>
                  <input type="submit" className="round-btn" id="upload-btn"/> 
                </form>
                {/* Info */}
                {this.state.files && <aside className="more-info">
                  <h4>Files</h4>
                  <ul className="simplified-list">{this.showFiles()}</ul>
                  <ul className="error-text simplified-list">{this.showErrors(fileRejections)}</ul>
                </aside>}
              </div>
            )}
          </Dropzone>
        </div>
      </div>
    );
  }
}
