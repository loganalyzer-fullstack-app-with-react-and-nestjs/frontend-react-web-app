import React from 'react';
import './App.css';
import {AuthForm} from './components/auth/authForm';
import { DisplayBox } from './components/mainPart/displayBox';
import { FileInput } from './components/mainPart/fileInput';
import { FilesLibrary } from './components/mainPart/filesLibrary';
import { HeaderNav } from './components/mainPart/headerNav';
import { SendRequestFactory } from './services/SendRequest';

const SEND_REAL_REQUESTS = true;
const requestObj = SendRequestFactory.create(SEND_REAL_REQUESTS);

class App extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      showLogin: true,
    }

    this.setVisibilityLogin = this.setVisibilityLogin.bind(this);
  }

  private setVisibilityLogin(render: boolean) {
    this.setState({
      showLogin: render
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.showLogin && <AuthForm isLogIn={true} requestObj={requestObj} setVisibility={this.setVisibilityLogin}/>}
          <HeaderNav left={
            <div className='Time-online-box'>
              <FileInput requestObj={requestObj} setVisibleLogin={this.setVisibilityLogin}></FileInput>
              <DisplayBox requestObj={requestObj} setVisibleLogin={this.setVisibilityLogin}></DisplayBox>
            </div>
          } right={
            <FilesLibrary requestObj={requestObj} setVisibleLogin={this.setVisibilityLogin}></FilesLibrary>
          }></HeaderNav>
        </header>
      </div>
    );
  }
}

export default App;
