import React from "react";
import './headerNav.css';

export class HeaderNav extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            checked: false
        }

        this.onClick = this.onClick.bind(this);
    }

    private onClick() {
        this.setState({
            checked: !this.state.checked,
        });
    }

    render() {
        return(
            <div className="header-box">
                <input type="checkbox" className="toggle-switch clickable" id="toggle-switch" onClick={this.onClick}/>
                {this.state.checked? this.props.right : this.props.left}
            </div>

        );
    }
}