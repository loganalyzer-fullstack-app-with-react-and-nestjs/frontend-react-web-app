import './navigationBar.css';

export function NavigationBar(props: any) {
    return(
        <span className="transparent-container" id="nav-container">
            {props.children}
        </span>
    );
}