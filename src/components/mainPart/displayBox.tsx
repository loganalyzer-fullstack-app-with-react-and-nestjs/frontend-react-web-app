import React from "react";
import { NavigationBar } from "./navigationBar";
import { RoundBtn } from "./roundBtn";

import './displayBox.css';
import { CONTENT_TYPE, REQUEST, SendRequest } from "../../services/SendRequest";

export class DisplayBox extends React.Component<{requestObj: SendRequest, setVisibleLogin(render: boolean): void}, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            result: null,
            error: null,
        }
    }

    private onClick(format: string) {
        const id = sessionStorage.getItem('fileId');
        if(!id) {
            this.setState({
                error: 'Upload file first!'
            });
            return;
        }

        const requestStr = `${process.env.REACT_APP_BACKEND_BASE_URL}${process.env.REACT_APP_GET_RESULTS_ROUTE}?id=${id}&format=${format}`;

        const url = new URL(requestStr);
        this.props.requestObj.sendRequest(url, REQUEST.GET, CONTENT_TYPE.DEF)
        .then(res => {
            //If successfull
            this.setState({
                result: { res: res.result, format: format },
                error: null
            });
        }).catch(err => {
            console.log(err);
            if(err.statusCode === 401)
              this.props.setVisibleLogin(true);
            return;
        });
    }

    render() {
        return(
            <div className="display-box">
                <NavigationBar>
                    <div className="btn-box">
                        <RoundBtn text={'JSON'} onClick={() => this.onClick('json')}></RoundBtn>
                        <RoundBtn text={'HTML'} onClick={() => this.onClick('html')}></RoundBtn>
                        <RoundBtn text={'XML'} onClick={() => this.onClick('xml')}></RoundBtn>
                    </div>
                </NavigationBar>
                <div className="transparent-container" id="display-port">
                    <div className="result-box">
                        {this.state.error}
                        {this.state.result?.format === 'html'? 
                            <div dangerouslySetInnerHTML={{ __html: this.state.result?.res }}>
                            </div> :
                            this.state.result?.res
                        }
                    </div>
                </div>
            </div>
        );
    }
}