import React from "react";
import { CONTENT_TYPE, REQUEST, SendRequest } from "../../services/SendRequest";
import './auth.css';

interface propsTransparentContainer {
    message: string;
    footerMessage: string;
    children: any;
    onClick(): void;
}

interface propsAuthForm {
    isLogIn: boolean;
    requestObj: SendRequest;
    setVisibility(render: boolean): void;
}

export class AuthForm extends React.Component<propsAuthForm, any> {
    constructor(public props: propsAuthForm) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isLogIn: props.isLogIn,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event: any) {
        const target = event.target as HTMLInputElement;
        this.setState({
            [target.name]: target.value,
        });
    }

    handleSubmit(event: any) {
        const requestStr = `${process.env.REACT_APP_BACKEND_BASE_URL}${(this.state.isLogIn)? process.env.REACT_APP_SIGNIN_ROUTE : process.env.REACT_APP_SIGNUP_ROUTE}`;
        event.preventDefault();

        const url = new URL(requestStr);
        this.props.requestObj.sendRequest(url, REQUEST.POST, CONTENT_TYPE.JSON, {
            email: this.state.email,
            password: this.state.password
        }).then(res => {
            //If successfull login
            this.props.setVisibility(false);
        }).catch(err => {
            console.log(err);
            return;
        });
    }

    handleClick() {
        this.setState({
            isLogIn: !this.state.isLogIn,
        });
    }

    render() {
        return (
            <div className="auth-container">
                {!this.state.isHidden? 
                    <TransparentContainer 
                        message={this.state.isLogIn? 'Log in' : 'Sign up'} 
                        footerMessage={this.state.isLogIn? 'Sign up' : 'Log in'} 
                        onClick={this.handleClick}>
                            
                        <form onSubmit={this.handleSubmit} className="general-form">
                            <input type="email" placeholder="Email" value={this.state.email} onChange={this.handleChange} name="email" />
                            <input type="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} name="password" />
                            <input type="submit" value="Submit" onSubmit={this.handleSubmit}/>
                        </form>
                    </TransparentContainer> 
                : null}
            </div>
        );
    }
}

export function TransparentContainer(props: propsTransparentContainer) {
    return (
        <div className="login-container">
            <div className="center">
                <div className="transparent-box login-box">
                    <h1 className="login-message">{props.message}</h1>
                    <div className="login-cont center">
                        {props.children}
                    </div>
                    <div id="login-footer">
                        <h4>Or <a id="signIn" onClick={props.onClick}>{props.footerMessage}</a></h4>
                    </div>
                </div>
            </div>
        </div>
    );
}