import React from "react";
import { CONTENT_TYPE, REQUEST, SendRequest } from "../../services/SendRequest";

import './filesLibrary.css';

export class FilesLibrary extends React.Component<{requestObj: SendRequest, setVisibleLogin(render: boolean): void}, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            result: null
        }

        this.renderFiles = this.renderFiles.bind(this);
    }

    componentDidMount() {
        const requestStr = `${process.env.REACT_APP_BACKEND_BASE_URL}${process.env.REACT_APP_USER_FILES_ROUTE}`;

        const url = new URL(requestStr);
        this.props.requestObj.sendRequest(url, REQUEST.GET, CONTENT_TYPE.DEF)
        .then(res => {
            //If successfull
            this.setState({
                result: res.files,
            });
        }).catch(err => {
            console.log(err);
            if(err.statusCode === 401)
                this.props.setVisibleLogin(true);
            return;
        });
    }

    renderFiles(): JSX.Element {
        const files = this.state.result;
        let arr = new Array<JSX.Element>();
        for(let file of files) {
            let el = (
                <div key={file.name} className="file-container">
                    <div className="file-label">
                        <span className="label">Name:</span>
                        <span>{file.name}</span>
                    </div>
                    <div className="file-label">
                        <span className="label">Date:</span>
                        <span>{file.createdAt}</span>
                    </div>
                    <div className="file-label">
                        <span className="label">File:</span>
                        <span>{file.content}</span>
                    </div>
                    <div className="file-label">
                        <span className="label">Result:</span>
                        <span>{file.parsed}</span>
                    </div>
                </div>
            );
            arr.push(el);
        }
        return (
            <div className="files-list">
                {arr}
            </div>
        );
    }

    render() {
        return(
            <div className="transparent-container files-box">
                {this.state.result?.length? 
                    this.renderFiles() :
                    <span>You don`t have any uploaded files yet</span>}
            </div>
        );
    }
}