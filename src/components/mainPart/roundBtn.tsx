interface propsRoundBtn {
    text: string;
    onClick(event: any): void;
}

export function RoundBtn(props: any) {
    return (
        <button className="round-btn" onClick={props.onClick}>{props.text}</button>
    );
}