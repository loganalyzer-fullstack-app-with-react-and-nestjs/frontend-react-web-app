export enum REQUEST {
    GET = 'GET',
    POST = 'POST',
}

export interface SendRequest {
    sendRequest(URL: URL, requestMethod: REQUEST, contentType: CONTENT_TYPE, requestBody?: object): Promise<any>;
}

export class SendRequestFactory {
    public static create(realRequest: boolean = true): SendRequest {
        if(realRequest)
            return new RealRequest();
        else 
            return new MockRequest();
    }
}

export enum CONTENT_TYPE {
    JSON = "application/json",
    FORM_DATA = "multipart/form-data",
    DEF = ''
}

export class RealRequest implements SendRequest {
    public async sendRequest(URL: URL, requestMethod: REQUEST, contentType: CONTENT_TYPE, requestBody?: object ): Promise<any> {
        const jwtToken = sessionStorage.getItem('JWT');
        const header = (jwtToken)? `Bearer ${jwtToken}`: undefined; 

        const options = this.getOptions(header, contentType, requestBody, requestMethod);

        let response = await fetch(URL.toString(), options);
        const result = await response.json();
        if (!response.ok) {
            this.showErrors(result);
            throw result;
        }
        if(result.accessToken)
            sessionStorage.setItem('JWT', result.accessToken);

        return result;
    }

    private showErrors(response: any) {
        alert(`${response.error} ${response.statusCode} ${response.message}`);
    }

    private getOptions(header: string | undefined, contentType: CONTENT_TYPE, requestBody: object | undefined, requestMethod: REQUEST) {
        let headers: any = {
        //   'Accept': '*/*',
          'Authorization': `${header}`,
        };
        let body: any;

        switch(contentType) {
            case CONTENT_TYPE.JSON:
                headers['Content-Type'] = CONTENT_TYPE.JSON;
                body = (requestBody !== undefined && requestMethod !== 'GET') ? JSON.stringify(requestBody) : null;
                break;

            case CONTENT_TYPE.FORM_DATA:
                body = requestBody as FormData;
        }

        return {
            method: requestMethod,
            body: body,
            headers: headers
        };
    }
}

// TODO
export class MockRequest implements SendRequest {
    public async sendRequest(URL: URL, requestMethod: REQUEST, contentType: CONTENT_TYPE, requestBody?: object): Promise<any> {
        return;
    }

}